#ifndef INTRO_SORT_H
#define INTRO_SORT_H

#include <functional>
#include <algorithm>
#include <math.h>    // floor :round up value
#include "shellsort.h"
#include "selectionsort.h"

namespace mylib {

template <class RandomAccessIterator,class Distance,class Compare = std::less<typename RandomAccessIterator::value_type>>

void introsort_loop( RandomAccessIterator first, RandomAccessIterator last, Distance depth_limit,Compare comp = Compare()) {

    static int __stl_threshold = 2e6;
    while (last-first > __stl_threshold){

        if(depth_limit==0){

            shellSort(first,last,comp);
            return;
        }
        --depth_limit;
        RandomAccessIterator cut= first+(last-first)/2;
        introsort_loop(cut,last,depth_limit,comp);
        last=cut;
    }

} // introsort_loop

template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
void introsort( RandomAccessIterator first, RandomAccessIterator last, Compare comp = Compare() ) {

    introsort_loop(first,last,2*floor(log(last-first)/log(2)),comp);
    selection_sort(first,last,comp);
}

} //mylib




#endif // INTRO_SORT_H
