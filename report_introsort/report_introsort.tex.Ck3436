% What kind of text document should we build
\documentclass[a4,10pt]{article}


% Include packages we need for different features (the less, the better)

% Clever cross-referencing
\usepackage{cleveref}

% Math
\usepackage{amsmath}
%code
\usepackage{listings}

% Algorithms
\usepackage{algorithm}
\usepackage{algpseudocode}

% Tikz
\RequirePackage{tikz}
\usetikzlibrary{arrows,shapes,calc,through,intersections,decorations.markings,positioning}

\tikzstyle{every picture}+=[remember picture]

\RequirePackage{pgfplots}









% Set TITLE, AUTHOR and DATE
\title{A benchmark of the introspective sorting algorithm applied to a custom vector using allocator}
\author{Nguyen, Thi Thanh Thao}
\date{\today}
 


\begin{document}



  % Create the main title section
  \maketitle

  \begin{abstract}
    This report is to introduce the tools and methods to implement the assignment 2 of the course STE6274 Game Design about introsort algorithm according to given templates.

  \end{abstract}


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%  The main content of the report  %%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
  \section{Introduction}
The task is implement the introspective sorting algorithm used in STL's sort method provided in <algorithm>.
Importantly, the implementation of the algorithm is described in the article: "Introspective Sorting and Selection Algorithms" by David R. Musser. 
\\As this sorting algorithm is a multi-step algorithm, each part of the algorithm can be timed and benchmarked. To benchmark the sorting algorithms, the program will apply the sorting algorithms to a container supporting random access iterators.
  \section{Introsort}
	According to the above article mentioned,Introsort(for Introspective sort), a new, hybrid sorting algorithm that behaves almost exactly like median-of-3 quicksort of most inputs but which is capable of detecting when partitioning is tending toward quadratic behavior. Moreover, the introsort achieves the same O(N log N) time bound as heapsort -another sorting algorithm introduced in that article also.
	
   \begin{algorithm}
    \caption{Introsort algorithm}\label{euclid}
    \begin{lstlisting}

template <class RandomAccessIterator,class Distance,class Compare = std::less<typename RandomAccessIterator::value_type>>

void introsort_loop( RandomAccessIterator first, RandomAccessIterator last, Distance depth_limit,Compare comp = Compare()) {

    static int __stl_threshold = 2e6;
    while (last-first > __stl_threshold){

        if(depth_limit==0){

            shellSort(first,last,comp);
            return;
        }
        --depth_limit;
        RandomAccessIterator cut= first+(last-first)/2;
        introsort_loop(cut,last,depth_limit,comp);
        last=cut;
    }

} // introsort_loop

template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type>>
void introsort( RandomAccessIterator first, RandomAccessIterator last, Compare comp = Compare() ) {

    introsort_loop(first,last,2*floor(log(last-first)/log(2)),comp);
    selection_sort(first,last,comp);
}

    \end{lstlisting}		
		
  \end{algorithm}

  \section{Benchmark set-up}
  Describe how the benchmark was performed. Be objective - don't describe too many details.
  The reader which this text is meant for should be able to reconstruct the experiment using his own tools.
  Some examples of topics for this section:
    \begin{itemize}
      \item All tests were performed on a computer with the following specifications...
      \item For the container we have measured how much time the CPU has spent on the \texttt{push\_back()} function. The emplace operation (construct and insert element) was used to allocate the storage.
      \item For the sorting algorithms, a data set of $N$ integers were sorted, where duration times were measured by using the high precision chrono device which is included with the STL.
    \end{itemize}

  \section{Results}

    \begin{figure}[width=\textwidth]
      \begin{tikzpicture}
        \begin{axis}[
          xmajorgrids=false,ymajorgrids=false,
          legend pos=north west,
          width=0.9\textwidth, height=0.25\textheight,
%          reverse legend
          ]

          \addplot[green] table[x=size ,y=time, skip first n=0] {dat/benchmark_stl_sort.dat};
          \addplot[red] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_bubble.dat};
          \addplot[blue] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_shell.dat};
          \addplot[pink] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_selection_sort.dat};
          \addplot[yellow] table[x=size ,y=time, skip first n=0] {dat/benchmark_intro_sort.dat};
          \legend{
            STL sort,  
            bubble,
            Shell,
            Selection,
            Intro
          }
        \end{axis}
      \end{tikzpicture} 
      \caption{My accurate and descriptive caption!}
      \label{fig:bench_sort}
    \end{figure} 

  \section{Concluding remarks}
  \begin{itemize}
    \item Reflect over the method and results.
    \item Topics for future work could be suggested here.
  \end{itemize}
  Example: By comparing the performance of the reserve and emplace functionality of the STL version of the vector to our custom vector implementation from \emph{mylib} we conclude that the STL vector is slightly faster at the specified operation.



  % Include the bibliography
  \bibliographystyle{plain}
  \bibliography{bibliography}

\end{document}
